# TITLES
# # FOR H1
## ## FOR H2
### ### FOR H3
#### #### FOR H4
##### ##### FOR H5
###### ###### FOR H6

# ORDERED LISTS
To create ordered lists you just have to put a number and a period at the beginning of each line of the list. The number is the order. For example: 1. Type your text here

1. Frist line
2. Second line

# UNORDERED LISTS
To create unordered lists you just have to put - at the beginning of each line of the list. For example: - Type your text here

- First line
- Second line

# SEPARATION LINES

if you want to make a separating line, just put --- in the place where you want the line to appear

---

# ENPHASIS IN TEXT
Add ** if you want **bold text**

Add _ if you want _italic text_

Add ** and _ if you want **_italic bold text_**

