# ABOUT THESE GUIDES

- These guides are not for learning. They are notes to quickly remember how things work.
- If you think something is wrong, please tell me and I'll change it.

---

# SOBRE ESTAS GUÍAS

- Estas guías no son para aprender. Son notas para recordar rápidamente cómo funcionan las cosas.
- Si crees que algo está mal, por favor dímelo y lo cambiaré.

