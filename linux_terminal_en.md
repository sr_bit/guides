# LINUX TERMINAL

- **Create folder:** mkdir folder_name
- **Enter in folder:** cd folder_name
- **Exit current folder:** cd ..
- **Delete folder:** rm -rf folder_name (this comand delete a folder and his content) (you need to stay out of folder to delete it)
- **Create file:** touch file_name
