# TÍTULOS
# UNA ALMOHADILLA PARA H1
## DOS ALMOHADILLAS PARA H2
### TRES ALMOHADILLAS PARA H3
#### CUATRO ALMOHADILLAS PARA H4
##### CINCO ALMOHADILLAS PARA H5
###### SEIS ALMOHADILLAS PARA H6

# LISTAS ORDENADAS
Para crear una lista ordenada solo tienes que poner un número y un punto al principi de cada línea. El número marca el orden. Por ejemplo: 1. Aquí va el texto

1. Primera línea
2. Segunda línea

# LISTAS SIN ORDEN
Para crear una lista sin orden solo tienes que poner - al principio de cada línea de la lista- Por ejemplo: - Aquí va el texto

- Primera línea
- Segunda línea

# LÍNEAS DE SEPARACIÓN

Si quieres una línea de separación, solo tienes que poner --- en el lugar donde quieres que aparezca dicha línea de separación

---


# ENFASIS EN EL TEXTO
Añade ** si quieres **texto en negrita**

Añade _ si quieres _texto cursiva_

Añade ** y _ si quires **_texto cursiva y en negrita_**
