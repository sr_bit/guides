# COMANDOS GIT

**git init ** Inicializa GIT en la carpeta actual

**git status ** Muestra el estado del repositorio (los archivos en rojo no tienen soporte y los archivos en verde sí tienen soporte)

**git add . ** Añade soporte a todos los archivos que no lo tengan

**git commit -m "Aquí tu comentario" ** Añade un comentario a nuevo soporte realizado

**git push " Actualiza el repositorio GitLab o GitHub con los archivos añadidos

---

### PASOS PARA CLONAR UN REPOSITORIO DE GITLAB
1. En GitLab, dentro del repositorio que queremos clonar, pulsamos el botón "CLONE" and y copiamos "Clone with HTTPS"

2. En tu ordenador, crea la estructura de carpetas necesaria y navega hasta la carpeta en la que quieres clonar el repositorio

3. Escribe **git clone [pega aquí el contenido del clipboard] (sin corchetes)

4. Si es necesario, puedes instalar las dependencias en este punto (normalmente en el archivo readme está la información)
