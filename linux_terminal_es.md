# TERMINAL LINUX

- **Crear carpeta:** mkdir nombre_de_la_carpeta
- **Entrar en una carpeta:** cd nombre_de_la_carpeta
- **Salir de la carpeta actual:** cd ..
- **Borrar carpeta:** rm -rf nombre_de_la_carpeta (este comando borra la carpeta y su contenido) (necesitas estar fuera de la capeta)
- **Crear un archivo:** touch nombre_del_archivo
