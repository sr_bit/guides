# GIT COMMANDS

**git init ** Initialize git in current folder

**git status ** Show status git (red files files without support and green files with support)

**git add . ** Add support to files without support

**git commit -m "Enter here your comment" ** Add comment to new supported files

**git push " Upload files to GitLab o GitHub

---

### STEPS TO CLONE GITLAB REPOSITORY IN LOCAL
1. In GitLab, inside repository to clone, press Clone button and copy Clone with HTTPS

2. On your computer, create structure folders necesary and navigate to folder you want to clone 
repository

3. Type **git clone [and paste from clipboard] (without brackets)

4. If necessary, you can install dependencies at this point (usually in the readme is this information)
