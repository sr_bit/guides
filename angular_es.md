# INSTALAR ANGULAR

Escribe: **npm install -g @angular/cli** para instalar Angular en tu ordenador

# CREAR UN NUEVO PROYECTO

Escribe: **ng new _proyect-name_** para crear un nuevo proyecto con Angular

# EJECUTAR LA APLICACIÓN

- Entra en la carpeta del proyecto
- Escribe: **ng serve -o** para ejecutar la aplicación en el puerto 4200 y abrir el navegador